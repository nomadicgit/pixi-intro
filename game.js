var Particle = function(props, ctrl) {
    // draw particle
    this.id = props.id
    this.element = new PIXI.Sprite.fromImage('assets/sprites/particle_'+ this.id+'.png');
    this.element.radius = 20;
    console.log(this.element.radius);
    this.element.position.x = props.basePosition.x - this.element.radius;
    this.element.position.y = props.basePosition.y - this.element.radius;
    // particle.pivot.set(300);s
    // make particle interactive
    this.element.interactive = true;

    // create label
    this.label = new PIXI.Container();
    var labelBg = new PIXI.Graphics();
    labelBg.beginFill(0xffffff, 1);
    labelBg.drawRoundedRect(0, 0, 20, 30, 1);
    this.label.addChild(labelBg);

    var labelTxt = new PIXI.Text(props.name, {
        fontFamily: 'Helvetica Neue',
        fontSize: 16,
        fontWeight: 700,
        fill: 0x333333,
        align: 'center'
    });
    labelTxt.y = labelBg.y + 5;
    labelBg.width = labelTxt.width + 20;
    labelTxt.x = labelBg.x + labelBg.width/2 - labelTxt.width/2;
    this.label.addChild(labelTxt);

    this.label.x = this.element.x + this.element.radius - this.label.width/2;
    this.label.y = this.element.y - this.label.height - 10;
    this.label.alpha = 0;
    this.label.scale.set(0.75);
    console.log(this.label);
    ctrl.game.stage.addChild(this.label);


    // add a 0 blur filter
    var blurFilter = new PIXI.filters.BlurFilter();
    blurFilter.blur = 0;
    this.element.filters = [blurFilter];

    this.hasCollided = () => {
        var circle1 = this.element;
        var circle2 = ctrl.hadron;

        var distanceX = circle1.x - circle2.x;
        var distanceY = circle1.y - circle2.y;
        var distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        var minDistance = circle1.radius + circle2.radius
        
        return distance < minDistance;
    }
    
    this.getSpinDirection = () => {
        return ctrl.spinningParticles.indexOf(this.spinOrder);
    }

    this.createSpeedBlur = () => {
        let radius = this.spinOrder === 0 ? 154 : 125;
        let blurRing = new PIXI.Graphics();
        blurRing.lineStyle(40, props.mainColor, 1);
        blurRing.beginFill(0xffffff, 0);
        blurRing.drawCircle(0, 0, radius);

        blurRing.x = ctrl.hadron.x;
        blurRing.y = ctrl.hadron.y;
        blurRing.alpha = 0;

        blurFilter = new PIXI.filters.BlurFilter();
        blurFilter.blur = 0;
        blurRing.filters = [blurFilter];
        
        this.blurRing = blurRing;
        ctrl.game.stage.addChild(this.blurRing);

        TweenMax.to(blurRing.filters[0], 4, {
            blur: 2,
            onComplete: () => {
                console.log(blurRing.filters[0], blurRing.filters[0].blur);
            }
        });
        TweenMax.to(this.element, 2, {
            alpha: 0.30
        });
        TweenMax.to(blurRing, 3, {
            alpha: 1,
            onComplete: () => {
                if (this.spinOrder === 1) setTimeout(() => ctrl.spinningComplete = true, 3000); 
            }
        });
    }
    
    this.onDragStart = (event) => {
        this.dragging = true;
        this.animate.label.move();
        this.animate.label.show();
        this.animate.scale();
        this.animate.move();

        if (ctrl.spinningParticles.length < 2) {
            ctrl.hadron.enabled();
        }
    }
    this.onDragEnd = (event) => {
        this.dragging = false;
        
        if (this.hasCollided() && ctrl.spinningParticles.length < 2) {
            let direction;
            if (this.spinOrder == undefined) {
                console.log('no spinorder', this.spinOrder);
                ctrl.spinningParticles.push(this.id);
                this.spinOrder = ctrl.spinningParticles.indexOf(this.id);
                console.log(this.spinOrder);
            }
            if (this.spinOrder) {
                this.direction = 'clockwise';
            } else {
                this.direction = 'counterclockwise';
            }
            this.animate.scale('down');
            this.animate.spin(this.spinOrder);
            setTimeout(this.createSpeedBlur.bind(this), 2500);
        } else {
            console.log('hasnt collided')
            this.isSpinning = false;
            console.log(ctrl.spinningParticles, this.spinOrder);
            if (this.spinOrder !== undefined) {
                ctrl.spinningParticles.splice(this.spinOrder, 1);
                this.spinOrder = undefined;
            }
            this.animate.return();
        }
        this.animate.label.hide();
        ctrl.hadron.enabled(false);
        
    }

    this.onDragMove = (event) => {
        const mouseX = event.data.global.x;
        const mouseY = event.data.global.y;
        if (this.dragging) {
            this.animate.move(mouseX, mouseY);
            this.animate.label.move(mouseX, mouseY);
        }
    }

    this.element
        .on('pointerdown', this.onDragStart)
        .on('pointerup', this.onDragEnd)
        .on('pointerupoutside', this.onDragEnd)
        .on('pointermove', this.onDragMove);

    this.animate = {
        spin: (clockwise) => {
            console.log(clockwise);
            let newX, newY;
            this.currentValue = 0;
            this.velocity = 0.01;
        
            if (clockwise) {
                newX = window.innerWidth/2 - this.element.radius + Math.sin(this.currentValue) * (160 - 40*this.spinOrder);
                newY = app.hadron.y - this.element.radius + Math.cos(this.currentValue) * (160 - 40*this.spinOrder);
            } else {
                newX = window.innerWidth/2 - this.element.radius + Math.cos(this.currentValue) * (160 - 40*this.spinOrder);
                newY = app.hadron.y - this.element.radius + Math.sin(this.currentValue) * (160 - 40*this.spinOrder);
            }
            console.log(newX);
            TweenMax.to(this.element.position, .5, {                                       
                x: newX,
                y: newY,
                ease: Power3.easeOut,
                onComplete: (elem) => {
                    this.isSpinning = true;
                }
            });
        },
        move: (xPos, yPos) => {
            TweenMax.to(this.element.position, .75, {
                x: xPos - this.element.radius - 10,
                y: yPos - this.element.radius - 10,
                ease: Back.easeOut
            });
        },
        scale: (down) => {
            if (!down) {
                TweenMax.to(this.element.scale, .75, {
                    x: 1.25,
                    y: 1.25,
                    ease: Back.easeOut
                });
            } else {
                TweenMax.to(this.element.scale, .75, {
                    x: 1,
                    y: 1,
                    ease: Back.easeOut
                });
            }
        },
        return: (event) => {
            TweenMax.to(this.element.scale, .75, {
                x: 1,
                y: 1,
                ease: Back.easeOut
            });
            TweenMax.to(this.element.position, .50, {
                x: props.basePosition.x - this.element.radius,
                y: props.basePosition.y - this.element.radius,
                ease: Back.easeOut,
                // onComplete: () => this.animate.label.returnToBase()
            });
        },
        label: {
            show: () => {
                TweenMax.to(this.label, 0.25, {
                    alpha: 1
                });
                TweenMax.to(this.label.scale, 0.5, {
                    x: 1,
                    y: 1,
                    ease: Power3.easeOut
                });
            },
            hide: () => {
                TweenMax.to(this.label, 0.25, {
                    alpha: 0,
                });
                TweenMax.to(this.label.scale, 0.5, {
                    x: 0.75,
                    y: 0.75,
                    ease: Power3.easeOut
                });
            },
            move: (mouseX, mouseY) => {
                let xPos = mouseX - this.label.width/2 - 5;
                let yPos = mouseY - this.label.height - 40;
                TweenMax.to(this.label.position, .75, {
                    x: xPos,
                    y: yPos,
                    ease: Back.easeOut,
                    onComplete: () => {
                        if (!this.isDragging) this.animate.label.returnToBase();
                    }
                });
            },
            returnToBase: () => {
                this.label.x = this.element.x + this.element.radius - this.label.width/2;
                this.label.y = this.element.y - this.label.height - 10;
            }
        }
    }
}

var Api = function() {
    let apiResponse;
    this.getParticles = function() {
        apiResponse = fetch(window.location+'particle_data.json')
                        .then((response) => response.json());

        return apiResponse;
    }
}

// console.log(new Api().getParticles());

var QRGame = function() {
    ctrl = this;
    this.init =  function(event) {
        ctrl.createVariables();
        ctrl.game = new PIXI.Application(window.innerWidth, window.innerHeight, {
            backgroundColor: 0x000000,
            transparent: true,
            antialias: true,
            // forceCanvas: true
        });
        document.body.appendChild(ctrl.game.view);
        // ctrl.createBackground();
        ctrl.getParticleData();
        ctrl.render();
        ctrl.createHadron();
    }

    this.getParticleData = function() {
        let data = new Api().getParticles().then((data) => {
        ctrl.particleData = data.particles;
        ctrl.createParticles(); 
        });
    }

    this.createVariables = function() {
        ctrl.particles = new Array();
        ctrl.spinningParticles = new Array();
        ctrl.hasCollided = false;
        ctrl.spinningComplete = false;
    }

    this.resize = function(event) {
        ctrl.game.view.width = window.innerWidth;
        ctrl.game.view.height = window.innerHeight;
    }

    this.createBackground = function () {
        ctrl.background = new PIXI.Graphics();
        ctrl.background.beginFill(0xff0092, 0);
        ctrl.background.drawRect(0, 0, window.innerWidth, window.innerHeight);
        ctrl.background.on('mousemove', ctrl.onMousemove);
        ctrl.game.stage.addChild(ctrl.background);
    }

    this.createParticles = function() {
        ctrl.particles = [];
        ctrl.draggingParticles = [];
        ctrl.spinningParticles = [];

        ctrl.particleData.forEach(function(particle, index) {
            ctrl.particles.push(new Particle({
                basePosition: {
                    // x: window.innerWidth/2 - (index * 60),
                    // y: window.innerHeight/4
                    x: 60,
                    y: index * 60 + 80
                },
                id: particle.id,
                name: particle.name,
                mainColor: particle.mainColor,
                sprite: particle.sprite
            }, ctrl));
        });
        ctrl.particles.forEach(particle => ctrl.game.stage.addChild(particle.element));
    }

    this.createHadron = function() {
        var hadron = new PIXI.Graphics();
        hadron.lineStyle(90, 0xffffff, 1);
        hadron.beginFill(0xffffff, 0.00);
        hadron.alpha = 0.15;
        hadron.drawCircle(0, 0, 140);
        // hadron.lineTo(window.innerWidth/2, window.innerHeight/2);
        hadron.x = window.innerWidth/2;
        hadron.y = window.innerHeight - 280,
        hadron.radius = hadron.width/2;
        ctrl.hadron = hadron;
        ctrl.game.stage.addChild(ctrl.hadron);

        ctrl.hadron.enabled = (enabled) => {
            TweenMax.to(ctrl.hadron, 0.5, {
                alpha: enabled === false  ? 0.1 : 0.5,
                ease: Sine.easeInOut
            });
        }
    }

    this.onMousemove = function(event) {
        ctrl.mouseX = event.data.global.x;
        ctrl.mouseY = event.data.global.x;
        console.log(ctrl.mouseX, ctrl.mouseY);
    }

    this.checkForCollision = (particleOuter, particleInner) => {
        // console.log(Math.round(particleOuter.element.x), Math.round(particleInner.element.x))
        // if (Math.round(particleOuter.element.x) == Math.round(particleInner.element.x)) {
        //     return true;
        // } else if (Math.round(particleOuter.element.y) == Math.round(particleInner.element.y)) {
        //     return true;
        // } else {
        //     return false;
        // }

        var circle1 = particleOuter.element;
        var circle2 = particleInner.element;

        var distanceX = circle1.x - circle2.x;
        var distanceY = circle1.y - circle2.y;
        var distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        var minDistance = circle1.radius + circle2.radius
        
        return distance < (minDistance + 1);
    }

    this.animationLoop = (delta) => {
        if (ctrl.spinningParticles.length == 2 && ctrl.spinningComplete) {
            ctrl.hasCollided = this.checkForCollision(ctrl.particles[ctrl.spinningParticles[0]], ctrl.particles[ctrl.spinningParticles[1]]);
        }

        if (!ctrl.hasCollided) {

            ctrl.particles.forEach((particle, index) => {
                if (particle.isSpinning) {
                    
                    if (particle.direction == 'clockwise') {
                        newX = window.innerWidth/2 - particle.element.radius + Math.sin(particle.currentValue) * (160 - 40*particle.spinOrder);
                        newY = app.hadron.y - particle.element.radius + Math.cos(particle.currentValue) * (160 - 40*particle.spinOrder);
                    } else {
                        newX = window.innerWidth/2 - particle.element.radius + Math.cos(particle.currentValue) * (160 - 40*particle.spinOrder);
                        newY = app.hadron.y - particle.element.radius + Math.sin(particle.currentValue) * (160 - 40*particle.spinOrder);
                    }
    
                    particle.velocity += 0.001;
                    particle.currentValue = particle.currentValue + Math.min(0.3, particle.velocity);
                    particle.radius += 0.5;
                    particle.element.x = newX;
                    particle.element.y = newY;
                }
            });
        } else {
            //animateCollision
            ctrl.drawCollision(ctrl.particles[ctrl.spinningParticles[1]]);
        }
    }

    this.drawCollision = (innerParticle) => {
        let startPosition = innerParticle.x;

        let curve = new PIXI.Graphics();
        curve.lineStyle(5, 0xffffff, 1);
        curve.quadraticCurveTo(startPosition, 0, startPosition+100, 200);
        ctrl.curve = curve;
        ctrl.game.stage.addChild(curve);
    }

    this.particlesAreSpinning= function() {
        let particles = 0;
        ctrl.particles.forEach((particle) => {
            if (particle.isSpinning) particles++;
        });

        return particles == 2;
    }

    this.render = function() {
        ctrl.game.ticker.add(ctrl.animationLoop);
    }
}
var app = new QRGame();
document.addEventListener('DOMContentLoaded', app.init);
window.addEventListener('resize', app.resize);